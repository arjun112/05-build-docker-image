# Use an official httpd runtime as a parent image
FROM httpd:latest

# Copy our custom configuration file into the container
#COPY ./my-httpd.conf /usr/local/apache2/conf/httpd.conf

# Expose port 80 for incoming HTTP traffic
EXPOSE 80

# Start the httpd server in the foreground when the container starts
CMD ["httpd", "-D", "FOREGROUND"]

